## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:


# **Delivery Service**

# **Routes/Links with description**

- Available to view even without token

 Route::get( list ,  UserController@list );

    - /api/list

-Returns all users.

 Route::post( login ,  API\UserController@login );

    - /api/login

-Required Fields

      - email
      - Password

Unique Token will be provided if the credentials provided are correct

 Route::post( register ,  API\UserController@register );

    - /api/register

-Required fields

- name - string
- email - string || email
- password
- role\_id - integer

- For account creation.

 Route::post( shortestPath ,  HomeController@shortestPath );

    - /api/shortestPath

-Required fields

- first - id/integer
- destination - id/integer
- basedOn - [time, cost]

- Following Routes/links that needs token in order to access it

Route::get(  ,  HomeController@all );

    - /api

-Returns All Points and its available Paths.

 Route::post( details ,  API\UserController@details );

    - /api/details

-Returns your user details

 Route::post( Point/Create ,  PointController@create );

    - /api/Point/Create 

-Field Required:

      - name - string

 Route::put( Point/Update/{id} ,  PointController@update );

    - /api/Point/Update/{id

-Field Required:

      - name - string

 Route::get( Point/Read ,  PointController@read );

    - /api/Point/Read 

-Returns All Points

 Route::delete( Point/Delete/{id} ,  PointController@delete );

    - /api/Point/Delete/{id}

-Deletes Point

 Route::post( Route/Create ,  RouteController@create );

    - /api/Route/Create

-Fields required:

      - point_id - integer
      - point_to - integer
      - cost - integer
      - time - integer

 Route::get( Route/Read ,  RouteController@read );

    - /api/Route/Read

-Returns All Routes

 Route::put( Route/Update/{id} ,  RouteController@update );

    - /api/Route/Update/{id}

-Fields Required

      - point_id - integer
      - point_to - integer
      - cost - integer
      - time - integer

 Route::delete( Route/Delete/{id} ,  RouteController@delete );

    - /api/Route/Delete/{id

-Deletes Route.
