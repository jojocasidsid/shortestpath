<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('list', 'UserController@list');



Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::post('shortestPath', 'HomeController@shortestPath');

Route::group(['middleware' => ['auth:api', 'admin']], function(){
Route::post('details', 'API\UserController@details');


Route::post('Point/Create', 'PointController@create');
Route::get('Point/Read', 'PointController@read');
Route::put('Point/Update/{id}', 'PointController@update');
Route::delete('Point/Delete/{id}', 'PointController@delete');
Route::get('', 'HomeController@all');
Route::post('Route/Create', 'RouteController@create');
Route::get('Route/Read', 'RouteController@read');
Route::put('Route/Update/{id}', 'RouteController@update');
Route::delete('Route/Delete/{id}', 'RouteController@delete');




});
