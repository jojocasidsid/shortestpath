<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    public function user(){
        return $this->belongsTo(User::class);   
      }


      public function route(){
        return $this->hasMany(Route::class);  
      }
}
