<?php

namespace App\Http\Controllers;

use App\Point;
use Illuminate\Http\Request;

use App\User;
use Auth;
use Validator;

class PointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(){

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $valid = Validator::make($request->all(),[
            'name' => 'required'
        ]);
        if($valid->fails()){
            return response()->json(
                ['error'=>$valid->errors()],
                401
            );
        }
        $point = new Point;
        $point->name = $request->name;
        $point->user_id = Auth::id();
         $point->save();


         return ['Result' => 'Successfully Created'];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function read()
    {
        return Point::all();
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Point  $point
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {

        $valid = Validator::make($request->all(),[
            'name' => 'required'
        ]);
        if($valid->fails()){
            return response()->json(
                ['error'=>$valid->errors()],
                401
            );
        }

        $point = Point::find($id);
        $point->name = $request->name;
        if($point->update()){
            return ['Result' => 'Successfully Updated'];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Point  $point
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $point = Point::find($id);
        if($point->delete()){
            return ['Result' => 'Successfully Deleted'];
        }
    }
}
