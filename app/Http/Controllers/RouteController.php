<?php

namespace App\Http\Controllers;

use App\Route;
use Illuminate\Http\Request;


use App\User;
use Auth;
use Validator;

class RouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $valid = Validator::make($request->all(),[
            'point_id' => 'required',
            'point_to' => 'required',
            'cost' => 'required',
            'time' => 'required',

        ]);
        if($valid->fails()){
            return response()->json(
                ['error'=>$valid->errors()],
                401
            );
        }
        $route = new Route;
        $route->point_id = $request->point_id;
        $route->point_to = $request->point_to;
        $route->cost = $request->cost;
        $route->time = $request->time;


    
        $route->save();


         return ['Result' => 'Successfully Created'];
    }


    public function read(){

        return Route::all();

    }

    /**
     * Store a newly created resource in storage.
     *



     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Route  $route
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        //

        $valid = Validator::make($request->all(),[
            'point_id' => 'required',
            'point_to' => 'required',
            'cost' => 'required',
            'time' => 'required',

        ]);
        if($valid->fails()){
            return response()->json(
                ['error'=>$valid->errors()],
                401
            );
        }
        $route =  Route::find($id);
        $route->point_id = $request->point_id;
        $route->point_to = $request->point_to;
        $route->cost = $request->cost;
        $route->time = $request->time;

     
    
        $route->update();


         return ['Result' => 'Successfully Updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Route  $route
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {

        $route = Route::find($id);
        if($route->delete()){
            return ['Result' => 'Successfully Deleted'];
        }
        
    }
}
