<?php

namespace App\Http\Controllers;

use App\Point;
use Illuminate\Http\Request;

use App\User;
use Auth;
use Validator;

use App\Route;


class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(){

        $points = Point::orderBy('id', 'asc')->get();
        $array = [];
        foreach ($points as $key => $value) {
            
            $value['id'];
            $routes = Route::where('point_id', $value['id'])->get();
            foreach ($routes as $key => $route) {
                $PointFind =  Point::find($route['point_to']);
               $array[] = [
                "Point" => $value['name'],
                "Routes" => $value['name'].'->'.$PointFind['name'],
                'Time' => $route['time'],
                'Cost' => $route['cost']
               ];
            }
        }

        return $array;


    }


    public function shortestPath(Request $request){

        $valid = Validator::make($request->all(),[
            'first_id' => 'required',
            'destination_id' => 'required',
            'basedOn' => 'required'
        ]);
        if($valid->fails()){
            return response()->json(
                ['error'=>$valid->errors()],
                401
            );
        }

        $first = $request->first_id;
        $destination = $request->destination_id;
        $basedOn = $request->basedOn;
        $navigator = 0;
        $query = Route::where('point_id', $first)->get();

        $_distArr = array();

        $query = Route::orderBy('point_id', 'desc')->get();
            foreach ($query as $key => $value) {
                $_distArr[$value['point_id']][$value['point_to']] = $value[  $basedOn ];
                $_distArr[$value['point_to']][$value['point_id']] = $value[  $basedOn ];
            }


                        $a = $first;
                        $b = $destination;


                        $S = array();
                        $Q = array();
                        foreach(array_keys($_distArr) as $val) {
                            $Q[$val] = 99999;
                        }
                        
                        $Q[$a] = 0;

                    while(!empty($Q)){
                        $min = array_search(min($Q), $Q);
                        if($min == $b){
                        break;
                        }
                        foreach($_distArr[$min] as $key=>$val) 
                        
                        
                        if(!empty($Q[$key]) && $Q[$min] + $val < $Q[$key]) {
                            $Q[$key] = $Q[$min] + $val;
                            $S[$key] = array($min, $Q[$key]);
                        }


                        unset($Q[$min]);
                    }

                    $path = array();
                    $pos = $b;
                    while($pos != $a){
                        $path[] = $pos;
                        $pos = $S[$pos][0];
                    }
                    $path[] = $a;
                    $path = array_reverse($path);

                    return [
                        "From" => $a,
                        "To" => $b,
                        "Path" => implode('->', $path),
                        "Total" => $S[$b][1],
                    ];
         
                  



    }



    
}
