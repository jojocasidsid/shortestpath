<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
      
      public function point(){
        return $this->belongsTo(Point::class);  
      }
}
